import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:two_counter_provider/counter_provider.dart';
import 'package:two_counter_provider/second_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => CounterProvider.instance,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {

  void gotoSecondScreen(BuildContext ctx){

    Navigator.of(ctx).push(MaterialPageRoute(builder: (_){
      return SecondScreen();
    }
    )
    );
  }
  @override
  Widget build(BuildContext context) {
    var itemData = Provider.of<CounterProvider>(context);
    final counterItem = itemData.counter;
    final otherCounter = itemData.otherCounter;
    return Scaffold(
      appBar: AppBar(
        title: Text('title'),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'counter = $counterItem',
              style: Theme.of(context).textTheme.headline4,
            ),
            Text(
              'other counter = $otherCounter',
              style: Theme.of(context).textTheme.headline4,
            ),
            FlatButton(child: Text('go to second screen'), onPressed: () => gotoSecondScreen(context),),
          ],
        ),
      ),

    );
  }
}

