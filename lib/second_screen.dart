import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'counter_provider.dart';

class SecondScreen extends StatelessWidget {
  final CounterProvider cp = CounterProvider.instance;
  final Random random = new Random(100);

  void _incrementCounter() {
    print('_incrementCounter ');

    if (random.nextInt(100) < 50) {
      cp.addCounter();
    } else {
      cp.addOtherCounter();
    }
  }

  void _decrementCounter() {
    print('_decrementCounter ');

    if (random.nextInt(100) < 50) {
      cp.removeCounter();
    } else {
      cp.removeOtherCounter();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('title'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            IconButton(icon: Icon(Icons.add), onPressed: _incrementCounter),
            IconButton(icon: Icon(Icons.remove), onPressed: _decrementCounter),
          ],
        ),
      ),
    );
  }
}
