import 'package:flutter/material.dart';

class CounterProvider with  ChangeNotifier {
  CounterProvider._privateConstructor();

  static final CounterProvider _instance = CounterProvider._privateConstructor();

  static CounterProvider get instance => _instance;

  int _counter = 0;
  int _otherCounter = 0;

  int get counter{
    return _counter;
  }

  int get otherCounter{
    return _otherCounter;
  }

  void addCounter(){
    print('addCounter ');
    _counter++;
    notifyListeners();
  }

  void addOtherCounter(){
    print('addOtherCounter ');
    _otherCounter++;
    notifyListeners();
  }

  void removeCounter(){
    print('removeCounter ');
    _counter--;
    notifyListeners();
  }

  void removeOtherCounter(){
    print('removeOtherCounter ');
    _otherCounter--;
    notifyListeners();
  }
}